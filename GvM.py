import time

#w = None
#g = None

class Welt:
    size_x = 0
    size_y = 0
    p = None
    #VOID = '·'
    #VOID = '▯'
    #VOID = '*'
    VOID = '░'
    WALL = '█'

    def __init__(self, size_x, size_y):
        #global w
        #w = self
        self.size_x = size_x
        self.size_y = size_y
        self.p = [[Welt.VOID] * self.size_y for i in range(self.size_x)]

    def __register__(self, g):
        self.g = g

    def set_point(self, x, y, symbol):
        self.p[x][y] = symbol

    def get_point(self, x, y):
        return self.p[x][y]

    def set_wall(self, x, y, richtung, anzahl):
        schritt_x = 0
        schritt_y = 0
        if richtung == "o":
            schritt_x = +1
        elif richtung == "w":
            schritt_x = -1
        elif richtung == "s":
            schritt_y = +1
        elif richtung == "n":
            schritt_y = -1
        else:
            raise Exception('Unbekannte Richtung "{}"'.format(richtung))
        for i in range(anzahl):
            self.set_point(x + schritt_x * i, y + schritt_y * i, self.WALL)

    def is_food(self, x, y):
        return 49 <= ord(self.p[x][y]) <= 57

    def is_wall(self, x, y):
        return self.p[x][y] == self.WALL

    def pop_food(self, x, y):
        if self.is_food(x, y):
            value = ord(self.p[x][y])
            if value == 49:
                self.p[x][y] = Welt.VOID
            else:
                self.p[x][y] = chr(ord(self.p[x][y]) - 1)
            return 1
        else:
            raise Exception('Dieses Feld ist kein Essen: "{}"'.format(self.p[x][y]))

    def plot(self):
        print('\x1bc')
        print(self.g)
        print()
        for y in range(self.size_y):
            for x in range(self.size_x):
                if self.g.get_pos() == (x, y):
                    print(self.g.get_symbol(), end='')
                else:
                    print(self.p[x][y], end='')
            print()
        print()
        time.sleep(0.1 if self.g.springt else 0.5)



class Guido:
    """Guido van Robot ist ein einfacher Hamster-Robotor, der in einer einfachen Roboterwelt lebt. Da
    Guido und seine Welt auf dem Bildschirm sichtbar sind, können wir beobachten, welche
    Wirkungen unsere Programmanweisungen auslösen.
    
    Parameter
    ---------
    w : Welt
        Die Roboterwelt in die Guido lebt. Diese muss vorher erzeugt werden.
    x, y : int
        Stelle auf die Guido gesetzt wird
    richtung : str
        Himmelsrichtung in die Guido schaut. Zulässige Werte: 's', 'o', 'n', 'w'
    futter : int
        Anzahl der Futterstücken in Guidos Hamserbacken
"""
    x = 0
    y = 0
    richtung = "o"
    futter = 0
    w = None
    springt = False

    def __init__(self, w, x, y, richtung, futter):
        #global g
        #g = self
        self.w = w
        self.x = x
        self.y = y
        self.richtung = richtung
        self.futter = futter
        self.w.__register__(self)

    def __str__(self):
        return 'Ich bin Guido auf Position x:{} y:{}, schaue in Richtung "{}" und habe {} Stück Futter'.format(self.x, self.y, self.richtung, self.futter)

    def get_pos(self):
        return (self.x, self.y)

    def get_symbol(self):
        if self.richtung == "o":
            return "▶" if self.springt else "▸"
        elif self.richtung == "w":
            return "◀" if self.springt else "◂"
        elif self.richtung == "s":
            return "▼" if self.springt else "▾"
        elif self.richtung == "n":
            return "▲" if self.springt else "▴"
        else:
            raise Exception('Unbekannte Richtung "{}"'.format(self.richtung))

    def __next_pos(self, richtung_abs):
        x = self.x
        y = self.y
        if richtung_abs == "o":
            x = self.x + 1
        elif richtung_abs == "w":
            x = self.x - 1
        elif richtung_abs == "s":
            y = self.y + 1
        elif richtung_abs == "n":
            y = self.y - 1
        else:
            raise Exception('Unbekannte Richtung "{}"'.format(richtung_abs))
        x = x % self.w.size_x
        y = y % self.w.size_y
        return (x, y)

    def __move(self):
        self.x, self.y = self.__next_pos(self.richtung)

    def __after_move(self):
        if self.w.get_point(self.x, self.y) == Welt.WALL:
            raise Exception('Guido ist leider gegen eine Wand gelaufen und tödlich verunglückt :-(')
        self.w.plot()

    def go(self):
        self.__move()
        self.__after_move()

    def jump(self, weite = 2):
        self.springt = True
        if weite < 2:
            raise Exception('Guido kann nicht so kurz springen (weite {})'.format(weite))
        for _ in range(weite - 1):
            self.__move()
            self.w.plot()
        self.__move()
        self.springt = False
        self.__after_move()

    def __next_richtung(self, richtung_abs, richtung_rel):
        if richtung_rel not in ('l', 'r'):
            raise Exception('Unbekannte relative Richtung "{}"'.format(self.richtung_rel))
        else:
            if richtung_abs == "o" and richtung_rel == "l" or richtung_abs == "w" and richtung_rel == "r":
                return("n")
            elif self.richtung == "w" and richtung_rel == "l" or richtung_abs == "o" and richtung_rel == "r":
                return("s")
            elif self.richtung == "s" and richtung_rel == "l" or richtung_abs == "n" and richtung_rel == "r":
                return("o")
            elif self.richtung == "n" and richtung_rel == "l" or richtung_abs == "s" and richtung_rel == "r":
                return("w")
            else:
                raise Exception('Unbekannte Richtung "{}"'.format(self.richtung_abs))

    def vorn_frei(self):
        next_pos = self.__next_pos(self.richtung)
        return not self.w.is_wall(next_pos[0], next_pos[1])

    def links_frei(self):
        next_pos = self.__next_pos(self.__next_richtung(self.richtung, 'l'))
        return not self.w.is_wall(next_pos[0], next_pos[1])

    def rechts_frei(self):
        next_pos = self.__next_pos(self.__next_richtung(self.richtung, 'r'))
        return not self.w.is_wall(next_pos[0], next_pos[1])

    def links_um(self):
        self.richtung = self.__next_richtung(self.richtung, 'l')
        self.w.plot()

    def rechts_um(self):
        self.richtung = self.__next_richtung(self.richtung, 'r')
        self.w.plot()

    def korn_da(self):
        """Prüft ob am aktuellen Feld ein Futter liegt

        Returns
        -------
        bool
            False wenn kein Futter, True sonst
        """
        return self.w.is_food(self.x, self.y)

    def nimm(self):
        """Guido hebt ein Futter auf. Fehler, wenn kein Futter vorhanden"""
        self.w.pop_food(self.x, self.y)
        self.futter = self.futter + 1
        self.w.plot()

#exec(open("./matthias.py").read())

