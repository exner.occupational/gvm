import requests
import json
try:
    from GuidoParameter import *
except ImportError:
    pass

hostName    = hostName if 'hostName' in vars() else "localhost"
serverPort  = serverPort if 'serverPort' in vars() else 8080

data = {}
#r = requests.post('http://' + hostName + ':' + str(serverPort) + '/rest/expose_interface', data = json.dumps(data))
r = requests.get('http://' + hostName + ':' + str(serverPort) + '/rest/interface.json')
#print(json.loads(r.text)["message"])
#print(json.loads(r.text)["traceback"])
interface = json.loads(r.content)

dyn = ""
for clazz in interface.keys():
    dyn += "class " + clazz + ":\n"
    dyn += "    \n"
    for func in interface[clazz].keys():
        #paramList = ", ".join(["self"] + interface[clazz][func])
        paramList = ", ".join(interface[clazz][func])
        dyn += "    def " + func + "(" + paramList + "):\n"
        dyn += "        params = locals()\n"
        dyn += "        del params['self']\n"
        if func == "__init__":
            dyn += "        self.id = " + interface[clazz][func][1] + "\n" # 1st patameter has to be the name/reference of the object
            dyn += "        r = requests.put('http://" + hostName + ":" + str(serverPort) + "/rest/" + clazz + "/' + self.id, data = json.dumps(params))\n"
            dyn += "        if r.status_code // 100 == 4:\n"
            dyn += "            raise Exception(r.content)\n"
        else:
            dyn += "        r = requests.post('http://" + hostName + ":" + str(serverPort) + "/rest/" + clazz + "/' + self.id + '/" + func + "', data = json.dumps(params))\n"
            dyn += "        if r.status_code // 100 == 4:\n"
            dyn += "            raise Exception(r.content)\n"
            dyn += "        return(json.loads(r.content))\n"
        #if func.startswith("is_"):
        #    dyn += "        return(json.loads(r.content))\n"
        #else:
        #    dyn += "        print(r.content)\n"
        dyn += "\n"

print(dyn)
exec(dyn)
print("Setup done")

