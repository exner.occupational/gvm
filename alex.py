# Welt erschaffen
Welt(20, 10)
w.set_point(6, 1, '9')
w.set_point(5, 2, '7')
w.set_point(3, 4, '4')
w.set_point(7, 7, '4')
w.set_wall(1, 5, 's', 4)
w.set_wall(5, 3, 'o', 4)

#Guido erschaffen
Guido(3, 7, 'o', 4)

# Guido anweisen
for _ in range(2):
    for _ in range(3):
        g.jump(2)
        while g.korn_da():
            g.nimm()
    if g.richtung == 'o':
        g.linksum()
        g.go()
        g.linksum()
    elif g.richtung == 'w':
        g.linksum()
        g.linksum()
        g.linksum()
        g.go()
        g.linksum()
        g.linksum()
        g.linksum()
  
