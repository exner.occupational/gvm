# -*- coding: utf-8 -*-
from GvRClient import Welt
from GvRClient import Guido
from random import randrange, choice

# Welt erschaffen
#w = GvM.Welt(25, 10)
WELT = "alexWelt"
GUIDO = "alex"
WIDTH = 30
HEIGHT = 20
OLIST = ["o", "s", "w", "s"]

#Guido erschaffen

g = Guido(GUIDO, WELT, 7, 6, 'o', 0)

# Guido anweisen
while True:
   if g.is_vorn_frei():
     g.go()
     if g.is_korn_da():
       g.nimm()
   else:
     g.links_um()

