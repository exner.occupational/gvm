# -*- coding: utf-8 -*-
from GvRClient import Welt
from GvRClient import Guido
from random import randrange, choice

# Welt erschaffen
#w = GvM.Welt(25, 10)
WELT = "alexWelt"
GUIDO = "alex"
WIDTH = 30
HEIGHT = 20
OLIST = ["o", "s", "w", "s"]
w = Welt(WELT, WIDTH, HEIGHT)

#           x  y  stück
#w.set_point(11, 6, '1')
#w.set_point(13, 6, '1')
#w.set_point(3, 4, '4')
#w.set_point(7, 7, '5')
#w.set_wall(5, 5, 'o', 13)

for _ in range(0, randrange(10, 20)):
    w.set_wall(randrange(0, WIDTH), randrange(0, HEIGHT), choice(OLIST), randrange(2, 10))

for _ in range(0, randrange(20, 50)):
    w.set_point(randrange(0, WIDTH), randrange(0, HEIGHT), choice(["1", "2", "5", "9"]))

#w.set_wall(5, 3, 'o', 4)

#Guido erschaffen
#g = Guido(GUIDO, WELT, 7, 6, 'o', 0)


# Guido anweisen
#for _ in range(2):
#  g.jump()
#g.nimm()
#g.jump()
#g.nimm()
