from http.server import BaseHTTPRequestHandler, HTTPServer
#from cgi import parse_header, parse_multipart
from urllib.parse import unquote, urlparse, parse_qs
from pathlib import PurePosixPath
import urllib.parse
import svgwrite
from svgwrite.container import Group
from datetime import datetime
from time import time
import inspect
from GvRServer import Welt
from GvRServer import Guido
import json
import re
import traceback
from string import Template
import xml.etree.ElementTree
try:
    from GuidoParameter import *
except ImportError:
    pass

hostName    = hostName if 'hostName' in vars() else "localhost"
serverPort  = serverPort if 'serverPort' in vars() else 8080
stateVersionsDict = {} # dict of dict: key Welt ref --> dict with key object type --> timestamp

def get_ts():
    return int(time() * 1000)

class Hack(str):
    def get_xml(self):
        return xml.etree.ElementTree.fromstring(str(self))

HTML = Template("""
<html>
  <head>
    <title>alex</title>
    <style>
      * {
        margin: 0;
        padding: 0;
      }
      .imgbox {
        display: grid;
        height: 100%;
        position: relative;
      }
      .center-fit {
        max-width: 100%;
        max-height: 100vh;
        margin: auto;
        position: absolute;
      }
    </style>
    <script src="/rest/${ref}/script.js" async></script>
  </head>
  <body>
    <div class="imgbox">
      <img class="center-fit" id="svgWelt" src="/rest/${ref}/state.svg?pane=Welt">
      <img class="center-fit" id="svgGuido" src="/rest/${ref}/state.svg?pane=Guido">
      <p id="msg">Statusleiste</p>
    </div>
  </body>
</html>
""")

SCRIPT = Template("""
    Welt = 0;
    Guido = 0;
    // Initialize
    window.onload = function()
    {
        last = 0;
        setInterval(getStateVersions, 300)
    }

    function getStateVersions()
    {
        var ajaxRequest = new XMLHttpRequest();
        ajaxRequest.onreadystatechange = function()
        {
            if(ajaxRequest.readyState == 4)
            {
                //the request is completed, now check its status
                if(ajaxRequest.status == 200)
                {
                    obj = JSON.parse(ajaxRequest.responseText);
                    if (obj.Welt > Welt) {
                        Welt = obj.Welt;
                        document.getElementById("msg").innerHTML = "Welt state: " + Welt;
                        refreshImage("svgWelt");
                    }
                    if (obj.Guido > Guido) {
                        Guido = obj.Guido;
                        document.getElementById("msg").innerHTML = "Guido state: " + Guido;
                        refreshImage("svgGuido");
                    }
		} else {
                    console.log("Status error: " + ajaxRequest.status);
                }
            }
        }
        ajaxRequest.open('GET', '/rest/${ref}/stateVersions.json');
        ajaxRequest.send();
    }

    function refreshImage(imgElement){
        // create a new timestamp
        var timestamp = new Date().getTime();
        var el = document.getElementById(imgElement);
        // 1) Attention: $$ is escaped single dollar sign
        // 2) URL has to look like /foo/bar/state.svg?pane=<pane>&t=<timestamp>
        var plainImgURL = el.src.replace(/&.*$$/, "")
        var queryString = "&t=" + timestamp;
        el.src = plainImgURL + queryString;
    } 
""")

Guido_functions = dict()
Welt_functions = dict()

# Create a single segment, add to drawing defs
def create_hello_world(dwg):
    # add Hello World
    text = dwg.text("Hello World",
                    insert = (1, 50),
                    fill = "rgb(55,0,0)",
                    style = "font-size:50; font-family:Arial")
    dwg.add(text)
    # add timestamp
    timeObj = datetime.now().time()
    timeStr = timeObj.strftime("%H:%M:%S.%f")
    text2 = dwg.text(timeStr,
                    insert = (1, 100),
                    fill = "rgb(55,0,0)",
                    style = "font-size:30; font-family:Arial")
    dwg.add(text2)

def get_frontpage():
    ret = "<html>\n"
    ret += "<body>\n"
    for w in Welt._instances.keys():
        ret += "<a href='http://" + hostName + ":" + str(serverPort) + "/rest/" + w + "/state.html'>" + w + "</a>\n"
        ret += "<br>\n"
    ret += "</body>\n"
    ret += "</html>\n"
    return ret
        
def paneWelt(dwg, welt):
    lines = welt.state().splitlines()
    y = 0
    for l in lines:
        x = 0
        for c in l:
            nested = dwg.svg(x = x * 100, y = y * 100,
                             width = 100, height = 100)
            char = dwg.text(c,
                            insert = (0, 85),
                            #x = 0.5, y = 0.5,
                            #dx = 0.5, dy = 0.5,
                            #y = "90%",
                            textLength = "100%",
                            #textHeight = "100%",
                            lengthAdjust = "spacingAndGlyphs",
                            fill = "rgb(55,0,0)",
                            opacity="0.5",
                            #style = "font-size-adjust:0.8"
                            style = "font-size:100; font-family:monospace"
                           )
            nested.add(dwg.rect(insert = (2, 2), size = (96, 96), fill="rgb(200,200,255)"))
            nested.add(char)
            dwg.add(nested)
            x += 1
        y += 1

def paneGuido(dwg, welt):
    guidos = welt.stateGuidos()
    #print(guidos)
    for g in guidos:
        (x, y) = g
        nested = dwg.svg(x = x * 100, y = y * 100,
        width = "100", height = "100")
        #print(nested.tostring())
        hack = Hack(guidos[g])
        nested.add(hack)
        dwg.add(nested)
    #print(dwg.tostring())
    #for y in range(size_y):
    #        for x in range(size_x):
    #            dwg.add(dwg.rect((x / size_x, y / size_y), (1 / size_x, 1 / size_y), stroke="red", fill="none"))
    #HEIGHT = 0.2
    #WIDTH = 0.2
    #g = svgwrite.container.Group(id = "alex")
    #g.add(dwg.circle((WIDTH/2, HEIGHT/2), HEIGHT/2))
    #g.add(dwg.circle((WIDTH/2, HEIGHT/2), HEIGHT/3))
    #dwg.defs.add(g)
    #return dwg.tostring()

# Create maximised SVG drawing
def create_svg(x, y):
    return svgwrite.Drawing("noname.svg",
            viewBox="0 0 {} {}".format(x, y),
            width="100%", height="100%",
            #width = x - 20 , height = y - 20,
            #style="border:solid 15px",
            debug=False)
 
def get_svg(ref, pane):
    welt = Welt.get_instance(ref)
    dwg = create_svg(welt.size_x * 100, welt.size_y * 100)
    if pane == "Welt":
        paneWelt(dwg, welt)
    elif pane == "Guido":
        paneGuido(dwg, welt)
    return dwg.tostring()

def expose_interface():
    #interface = dict()
    ##Welt
    ##====
    #wfunctions = dict()
    #wfunctions["set_point"] = ["x", "y", "symbol"]
    #wfunctions["get_point"] = ["x", "y"]
    ##...
    #interface["Welt"] = wfunctions
    ##Guido
    ##=====
    #gfunctions = dict()
    #gfunctions["go"] = []
    #gfunctions["jump"] = ["length"]
    ##...
    #interface["Guido"] = gfunctions
    #
    interface = dict()
    wfunctions = dict()
    for f in Welt_functions.keys():
        paramlist = []
        s = inspect.signature(Welt_functions[f])
        for p in s.parameters.values():
            paramlist.append(p.name)
        wfunctions[f] = paramlist
    interface["Welt"] = wfunctions
    gfunctions = dict()
    for f in Guido_functions.keys():
        paramlist = []
        s = inspect.signature(Guido_functions[f])
        for p in s.parameters.values():
            paramlist.append(p.name)
        gfunctions[f] = paramlist
    interface["Guido"] = gfunctions
    return interface

class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        parsed = urlparse(self.path)
        pp = PurePosixPath(unquote(parsed.path)).parts
        pplen = len(pp)
        qs = parse_qs(parsed.query)
        #print(pplen)
        #print(pp)
        #print(qs)
        # /
        if pp == ("/",):
            self.send_response(302)
            self.send_header('Location', "/index.html")
            self.end_headers()
        # /index.html
        elif pp == ("/", "index.html"):
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.send_header('Cache-Control', 'no-store, must-revalidate')
            self.send_header('Expires', '0')
            self.end_headers()
            self.wfile.write(bytes(get_frontpage(), "utf-8"))
        # /rest/<Welt ref>/state.html
        elif pplen == 4 and pp[0:2] == ("/", "rest") and pp[3] == "state.html":
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes(HTML.substitute({'ref' : pp[2]}), "utf-8"))
        # /rest/<Welt ref>/script.js
        elif pplen == 4 and pp[0:2] == ("/", "rest") and pp[3] == "script.js":
            self.send_response(200)
            self.send_header("Content-type", "application/javascript")
            self.end_headers()
            self.wfile.write(bytes(SCRIPT.substitute({'ref' : pp[2]}), "utf-8"))
        # /rest/interface.json
        elif pp == ("/", "rest", "interface.json"):
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(bytes(json.dumps(expose_interface()), "utf-8"))
        # /rest/<Welt ref>/stateVersion.json
        elif pplen == 4 and pp[0:2] == ("/", "rest") and pp[3] == "stateVersions.json":
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(bytes(json.dumps(stateVersionsDict[pp[2]]), "utf-8"))
        # /rest/<Welt ref>/state.svg
        elif pplen == 4 and pp[0:2] == ("/", "rest") and pp[3].startswith("state.svg"):
            if "pane" in qs:
                self.send_response(200)
                self.send_header("Content-type", "image/svg+xml")
                self.end_headers()
                self.wfile.write(bytes(get_svg(pp[2], qs["pane"][0]), "utf-8")) # remove ?t=4234 part (workaround to disable caching)
            else:
                self.send_response(404)
                self.send_header("Content-type", "image/svg+xml")
                self.end_headers()
                self.wfile.write(bytes("", "utf-8"))
        else:
            self.send_response(404)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes("unknown endpoint", "utf-8"))

    def do_PUT(self):
        parsed = urlparse(self.path)
        pp = PurePosixPath(unquote(parsed.path)).parts
        pplen = len(pp)
        #qs = parse_qs(parsed.query)
        content_len = int(self.headers.get('content-length'))
        post_body = self.rfile.read(content_len)
        # /rest/Welt/blah123" with parameters as JSON in request body
        # /rest/Guido/blah123" with parameters as JSON in request body
        if pplen == 4 and (pp[0:3] == ("/", "rest", "Welt") or pp[0:3] == ("/", "rest", "Guido")):
            ref = pp[3] # e. g. "blah123"
            data = json.loads(post_body)
            o = globals()[pp[2]] # get object
            content = json.dumps(str(o(**data))) # create the object
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(bytes(content, "utf-8"))
            if pp[2] == "Welt":
                stateVersionsDict[pp[3]] = {"Welt": get_ts(), "Guido": get_ts()} # create dict (of object types) within dict (of Welt refs)
                #print(stateVersionsDict)
            elif pp[2] == "Guido":
                i = o.get_instance(ref) # get instance of object
                stateVersionsDict[i.w.welt_ref]["Guido"] = get_ts()


    def do_POST(self):
        parsed = urlparse(self.path)
        pp = PurePosixPath(unquote(parsed.path)).parts
        pplen = len(pp)
        #qs = parse_qs(parsed.query)
        content_len = int(self.headers.get('content-length'))
        post_body = self.rfile.read(content_len)
        print(post_body)
        # POST requests like "http://localhost:8080/rest/Welt/blah123/set_point" with parameters as JSON in request body
        if pplen == 5 and pp[0:2] == ("/", "rest") and ((pp[2] == "Welt" and pp[4] in Welt_functions)
                                                     or (pp[2] == "Guido" and pp[4] in Guido_functions)):
            try:
                ref = pp[3] # e. g. "blah123"
                data = json.loads(post_body)
                o = globals()[pp[2]] # get object
                i = o.get_instance(ref) # get instance of object
                f = getattr(i, pp[4]) # get the function of instance
                content = json.dumps(f(**data))
                self.send_response(200)
                self.send_header("Content-type", "application/json")
                self.end_headers()
                self.wfile.write(bytes(content, "utf-8"))
                # let HTML page know that State of Welt or Guido has changed
                if pp[2] == "Welt":
                    stateVersionsDict[ref]["Welt"] = get_ts()
                if pp[2] == "Guido":
                    stateVersionsDict[i.w.welt_ref]["Guido"] = get_ts()
                if pp[4] == "nimm":
                    stateVersionsDict[i.w.welt_ref]["Welt"] = get_ts()
                #print(stateVersionsDict)
            except Exception as e:
                traceback_str = ''.join(traceback.format_tb(e.__traceback__))
                self.send_response(400)
                self.send_header("Content-type", "application/json")
                self.end_headers()
                content = json.dumps({"traceback": traceback_str, "message": str(e)})
                self.wfile.write(bytes(content, "utf-8"))
                # example of the other side:
                # import requests
                # import json
                # data = {'x':7, 'y':7, 'richtung':'x', 'anzahl': 5}
                # r = requests.post('http://localhost:8080/rest/Welt/set_wall', data = json.dumps(data))
                # print(json.loads(r.text)["message"])
                # print(json.loads(r.text)["traceback"])
        else:
            self.send_response(404)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(bytes("unknown endpoint", "utf-8"))



if __name__ == '__main__':

    for m in inspect.getmembers(Guido):
        f = m[1]
        if (inspect.isfunction(f) and (not (f.__name__.startswith("__")) or f.__name__ == ("__init__") )):
            Guido_functions[f.__name__] = f

    for w in inspect.getmembers(Welt):
        f = w[1]
        if (inspect.isfunction(f) and (not (f.__name__.startswith("__")) or f.__name__ == ("__init__") )):
            Welt_functions[f.__name__] = f

#    for m in inspect.getmembers(w):
#        f = m[1]
#        #if (inspect.ismethod(f) and not (f.__name__.startswith("__"))):
#        if (inspect.ismethod(f) and (not (f.__name__.startswith("__")) or f.__name__ == ("__init__") )):
#            Welt_functions[f.__name__] = f

    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
 
# EOF

