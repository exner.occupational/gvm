# Welt erschaffen
Welt(20, 14)

w.set_wall(5, 13, 'n', 3)
w.set_wall(5, 10, 'w', 3)
w.set_wall(2, 10, 'n', 3)
w.set_wall(3, 8, 'o', 6)
w.set_wall(8, 8, 's', 3)
w.set_wall(8, 11, 'o', 5)
w.set_wall(12, 11, 'n', 8)
w.set_wall(12, 3, 'w', 5)
w.set_wall(7, 4, 'n', 2)
w.set_wall(7, 5, 'w', 3)
w.set_wall(4, 5, 'n', 4)
w.set_wall(4, 1, 'w', 5)

#w.set_point(6, 1, '9')

# Guido erschaffen
Guido(1, 9, 'n', 1)

# Guido anweisen

# Gehe bis Wand
while g.vorn_frei():
    g.go()
g.linksum()
# Jetzt weiß Guido, dass rechts von ihm eine Wand ist

while True:
    if g.rechts_frei():
        g.rechtsum()
        g.go()
    elif not g.vorn_frei():
        g.linksum()
    else:
        g.go()
